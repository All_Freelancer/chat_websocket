import 'package:flutter/material.dart';

import 'package:flutter/foundation.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

//****************************** CLASS CHAT SCREEN *****************************
class ChatScreen extends StatefulWidget {
  final String title;
  final WebSocketChannel channel;

  //:::::::::::::::::::::::::::: CONSTRUCT ::::::::::::::::::::::::::::
  ChatScreen ({Key key, @required this.title, @required this.channel})
      : super(key: key);


  //:::::::::::::::::::::::::::: CREATE STATE :::::::::::::::::::::::::
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

//********************************** STATE CHAT SCREEN *************************
class _ChatScreenState extends State<ChatScreen> {

  TextEditingController _controller = TextEditingController();

  //***************************** INIT STATE *********************************
  /*
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    widget.channel.stream.listen(
        (message){
          // handling of the incoming messages
        },
        onError: function(error, StackTrace stackTrace){
           // error handling
        },
        onDone: function(){
          // communication has been closed
        }
    );
  }
  */
  //*******************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),


      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[

            Form(
              child: TextFormField(
                controller: _controller,
                decoration: InputDecoration(labelText: 'Send a message'),
              ),
            ),

            StreamBuilder(
              stream: widget.channel.stream,

              builder: (context, snapshot) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 24.0),
                  child: Text(snapshot.hasData ? '${snapshot.data}' : ''),
                );
              },
            )
          ],
        ),
      ),

      //--------------------------- SEND MESSAGE -------------------------
      floatingActionButton: FloatingActionButton(
        onPressed: _sendMessage,
        tooltip: 'Send message',
        child: Icon(Icons.send),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void _sendMessage() {
    if (_controller.text.isNotEmpty) {
      setState(() {
        widget.channel.sink.add(_controller.text);
      });

    }
  }

  @override
  void dispose() {
    widget.channel.sink.close();
    super.dispose();
  }

}

//******************************************************************************
